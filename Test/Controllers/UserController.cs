﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.Models;

namespace Test.Controllers
{
    public class UserController : Controller
    {        
        public ActionResult List(string CitySelect, SortOrder AgeSelect = SortOrder.None)
        {
            var db = new UserModel();
            var users = new List<UserProfile>();
            var cityList = db.UserProfiles.Select(x => x.City).Distinct().ToList();


            if (!String.IsNullOrEmpty(CitySelect))
            {
                users = db.UserProfiles.Where(x => x.City == CitySelect).ToList();
            }
            else {
                users = db.UserProfiles.ToList();
            }

            if (!users.Any())
            {
                db.UserProfiles.Add(new UserProfile { Name = "Саша", City = "Київ", Age = 18 });
                db.UserProfiles.Add(new UserProfile { Name = "Аня", City = "Львів", Age = 25 });
                db.UserProfiles.Add(new UserProfile { Name = "Віка", City = "Харьків", Age = 22 });
                db.UserProfiles.Add(new UserProfile { Name = "Ігор", City = "Київ", Age = 40 });
                db.UserProfiles.Add(new UserProfile { Name = "Ваня", City = "Львів", Age = 16 });

                db.SaveChanges();
            }

            switch (AgeSelect)
            {
                case SortOrder.Ascending:
                    {
                        users = users.OrderBy(x => x.Age).ToList();
                        break;
                    }
                case SortOrder.Descending:
                    {
                        users = users.OrderByDescending(x => x.Age).ToList();
                        break;
                    }
            }

            var vm = new ListViewModel
            {
                CitySelect = CitySelect,
                AgeSelect = AgeSelect,
                users = users,
                CityList = cityList
            };

            

            return View(vm);
        }
    }
}
