﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class ListViewModel
    {
        public string CitySelect { get; set; }
        public SortOrder? AgeSelect { get; set; }
        public List<UserProfile> users { get; set; }
        public List<string> CityList { get; set; }

    }

    public enum SortOrder {
        Ascending = 0,
        Descending = 1,
        None = 3
    }
}